FROM python:3.8-slim-buster

COPY headercheck.py headercheck.py
COPY requirements.txt requirements.txt


# Download latest listing of available packages:
RUN apt-get -y update
# Upgrade already installed packages:
RUN apt-get -y upgrade
# Install a new package:
RUN apt-get -y install git

RUN pip install -r requirements.txt

ENTRYPOINT [""]