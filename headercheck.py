# +--------------------------------------------------------------------------+
# |  Licensed Materials - Property of GitLab, Inc.                           |
# |                                                                          |
# | (C) Copyright GitLab, Inc 2020                                           |
# +--------------------------------------------------------------------------+
# | This module  Licensed under the Apache License, Version 2.0 (the         |
# | "License"); you may not use this file except in compliance with the      |
# | License.                                                                 |
# | You may obtain a copy of the License at                                  |
# | http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable |
# | law or agreed to in writing, software distributed under the License is   |
# | distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY |
# | KIND, either express or implied. See the License for the specific        |
# | language governing permissions and limitations under the License.        |
# +--------------------------------------------------------------------------+
# | Author: Tim Poffenbarger
# +--------------------------------------------------------------------------+

import glob
import os
import sys
import tempfile

from envparse import Env


env = Env()
if os.path.isfile('.env'):
    env.read_envfile('.env')


def anywhere_in(needle, haystack):
    n = len(needle)
    if needle[0] not in haystack or len(haystack) < n:
        return False
    return any(needle == haystack[i:i + n] for i in range(len(haystack) - n + 1))


def perform_insert(prefix_file_path, file_path):
    new_file, filename = tempfile.mkstemp()
    with open(filename, 'w') as new:
        with open(prefix_file_path) as prefix:
            new.write(prefix.read())
        with open(file_path) as old:
            new.write(old.read())
    with open(file_path, 'w') as old:
        with open(new_file) as new:
            old.write(new.read())


def header_insert_check(file_type, prefix_file_path, insert_if_not_there=False, ignore_middle_of_file_finds=False):
    """
    used to start doing the evaluation
    """
    temp_file = '/tmp/project_modified_' + env('CI_JOB_ID')
    with open(prefix_file_path) as prefix:
        prefix_content = prefix.read().splitlines(False)

    for content_file_path in glob.glob(f"*.{file_type}"):
        insert = False
        with open(content_file_path) as file:
            file_content = file.read().splitlines(False)
            if file_content[:len(prefix_content)] == prefix_content:
                sys.stdout.write('- ' + content_file_path + '\n')
                continue
            elif anywhere_in(prefix_content, file_content):
                sys.stdout.write('= ' + content_file_path + '\n')
                sys.stderr.write('Invalid consideration as content is not at the top of file\n')
                sys.stdout.flush()
                sys.stderr.flush()
                if not ignore_middle_of_file_finds:
                    sys.exit(1)
            elif insert_if_not_there:
                insert = True
        if insert:
            perform_insert(prefix_file_path, content_file_path)
            sys.stdout.write('+ ' + content_file_path + '\n')
            if not os.path.isfile(temp_file):
                with open(temp_file, 'w') as f:
                    f.write('')

    sys.stdout.flush()
    sys.stderr.flush()


def main():
    """
    Used to begin the invocation of this script
    """

    header_insert_check(
        file_type=env('HEADER_INSERT_FILE_TYPE'),
        prefix_file_path=env('HEADER_INSERT_PREFIX_FILE_PATH'),
        insert_if_not_there=True,
        ignore_middle_of_file_finds=False,
    )


if __name__ == '__main__':
    main()
