# Header Insert

Look for files, if the text is not in there, then insert, commit, push.


## Getting Started

Generate a SSH Public/Private Key Pair: `ssh-keygen -t rsa`

Add the Public Key as a Deploy Key with **Write Access** in Repository Settings

![image](images/deploy_key.png)

Add them as **files** within your GitLab CI Variables Settings section

![image](images/ci_settings.png)

Simply add this into your `.gitlab-ci.yml`
```yaml
variables:
  HEADER_INSERT_FILE_TYPE: py
  # prefix.txt will be the relative file path in your git repository of the prefix file
  HEADER_INSERT_PREFIX_FILE_PATH: prefix.txt

include:
- remote: "https://gitlab.com/poffey21/header-insert/-/raw/master/Headers.gitlab-ci.yml"
```
